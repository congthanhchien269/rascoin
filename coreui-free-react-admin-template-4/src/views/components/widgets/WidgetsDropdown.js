import React, { useEffect, useState } from 'react'
import { domain } from '../../../assets/constant.js'
import {
  CRow,
  CCol,
  CDropdown,
  CDropdownMenu,
  CDropdownItem,
  CDropdownToggle,
  CWidgetStatsA,
} from '@coreui/react'

import axios from 'axios'
import { getStyle } from '@coreui/utils'
import { CChartBar, CChartLine } from '@coreui/react-chartjs'
import CIcon from '@coreui/icons-react'
import { cilArrowBottom, cilArrowTop, cilOptions } from '@coreui/icons'

function formatDate(date) {
  var dd = date.getDate()
  var mm = date.getMonth() + 1
  var yyyy = date.getFullYear()
  if (dd < 10) {
    dd = '0' + dd
  }
  if (mm < 10) {
    mm = '0' + mm
  }
  date = mm + '/' + dd + '/' + yyyy
  return date
}

function Last7Days() {
  var result = []
  for (var i = 0; i < 7; i++) {
    var d = new Date()
    d.setDate(d.getDate() - i)
    result.push(formatDate(d))
  }

  return result.join(',')
}

function getLast7DaysDateString(dates) {
  return dates.split(',')
}

function getLast7DaysTimestamp(dates, inSecond) {
  dates = dates.split(',')
  let results = []
  dates.forEach((day) => {
    if (inSecond) {
      results.push(parseInt(new Date(day).getTime() / 1000))
    } else {
      results.push(new Date(day).getTime())
    }
  })
  return results
}

function getCurrentTimestamp(inSecond = false) {
  const currentDate = new Date()
  const timestamp = currentDate.getTime()
  if (inSecond) {
    return parseInt(timestamp / 1000)
  } else {
    return timestamp
  }
}

const WidgetsDropdown = () => {
  let defaultPriceHistory = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'bitcoin',
        backgroundColor: 'transparent',
        borderColor: 'rgba(255,255,255,.55)',
        pointBackgroundColor: getStyle('--cui-primary'),
        data: [65, 59, 84, 84, 51, 55, 40],
      },
    ],
  }
  const header = {
    key: 'access_token',
    value: 'ujyicmbpwraggefwsedvdovbbympvqfpjthmdwbvuxjlewkwqxhpzyqvlrzvbljw',
    type: 'text',
  }

  const fetchPriceHistory = async (header, coin_id = 1267) => {
    let last7DaysString = Last7Days()
    let last7DaysDates = getLast7DaysDateString(last7DaysString)
    let last7DaysTimestampSecond = getLast7DaysTimestamp(last7DaysString, true)
    let dateEnd = last7DaysTimestampSecond[0]
    let dateStart = last7DaysTimestampSecond[last7DaysTimestampSecond.length - 1]
    let query = {
      filters: {
        $and: [
          { timestamp: { $gte: dateStart } },
          { timestamp: { $lte: dateEnd } },
          { coin_id: { $eq: coin_id } },
        ],
      },
    }
    let query_string = JSON.stringify(query)
    const response = await axios.get(`${domain}/api/v1/crypto_price?q=${query_string}`, header)
    let data = defaultPriceHistory
    data.labels = last7DaysDates
    console.log('data', data)
    data.datasets[0].data = response.data.objects.map((price) => price.price)
    // data.datasets[0].data = console.log('data.datasets', data.datasets[0].data)
    console.log('data', data)
    setPriceHistory(data)
  }

  const fetchCurrentCryptoPrice = async (coin_id = 'binancecoin') => {
    const response = await axios.get(`${domain}/current_price?coin_id=${coin_id}`)
    console.log(response)
    let data = response.data.price
    console.log('current price', data)
    if (coin_id === 'binancecoin') {
      setcurrentBNBPrice(data)
    }
    if (coin_id === 'cardano') {
      setcurrentADAPrice(data)
    }
    if (coin_id === 'ripple') {
      setcurrentXRPPrice(data)
    }
    if (coin_id === 'ethereum') {
      setcurrentETHPrice(data)
    }
  }

  const [priceHistory, setPriceHistory] = useState(defaultPriceHistory)
  const [currentBNBPrice, setcurrentBNBPrice] = useState(0)
  const [currentADAPrice, setcurrentADAPrice] = useState(0)
  const [currentXRPPrice, setcurrentXRPPrice] = useState(0)
  const [currentETHPrice, setcurrentETHPrice] = useState(0)

  useEffect(() => {
    fetchPriceHistory(header)
    fetchCurrentCryptoPrice('binancecoin')
    fetchCurrentCryptoPrice('cardano')
    fetchCurrentCryptoPrice('ripple')
    fetchCurrentCryptoPrice('ethereum')
  }, [])

  return (
    <CRow>
      <CCol sm={6} lg={3}>
        <CWidgetStatsA
          className="mb-4"
          color="primary"
          value={
            <>
              {'$' + currentBNBPrice.toString()}{' '}
              <span className="fs-6 fw-normal">
                (-12.4% <CIcon icon={cilArrowBottom} />)
              </span>
            </>
          }
          title="Binance Coin"
          action={
            <CDropdown alignment="end">
              <CDropdownToggle color="transparent" caret={false} className="p-0">
                <CIcon icon={cilOptions} className="text-high-emphasis-inverse" />
              </CDropdownToggle>
              <CDropdownMenu>
                <CDropdownItem>Action</CDropdownItem>
                <CDropdownItem>Another action</CDropdownItem>
                <CDropdownItem>Something else here...</CDropdownItem>
                <CDropdownItem disabled>Disabled action</CDropdownItem>
              </CDropdownMenu>
            </CDropdown>
          }
          chart={
            <CChartLine
              className="mt-3 mx-3"
              style={{ height: '70px' }}
              data={priceHistory}
              options={{
                plugins: {
                  legend: {
                    display: false,
                  },
                },
                maintainAspectRatio: false,
                scales: {
                  x: {
                    grid: {
                      display: false,
                      drawBorder: false,
                    },
                    ticks: {
                      display: false,
                    },
                  },
                  y: {
                    min: 30,
                    max: 89,
                    display: false,
                    grid: {
                      display: false,
                    },
                    ticks: {
                      display: false,
                    },
                  },
                },
                elements: {
                  line: {
                    borderWidth: 1,
                    tension: 0.4,
                  },
                  point: {
                    radius: 4,
                    hitRadius: 10,
                    hoverRadius: 4,
                  },
                },
              }}
            />
          }
        />
      </CCol>
      <CCol sm={6} lg={3}>
        <CWidgetStatsA
          className="mb-4"
          color="info"
          value={
            <>
              {'$' + currentADAPrice.toString()}{' '}
              <span className="fs-6 fw-normal">
                (40.9% <CIcon icon={cilArrowTop} />)
              </span>
            </>
          }
          title="Cardano ADA"
          action={
            <CDropdown alignment="end">
              <CDropdownToggle color="transparent" caret={false} className="p-0">
                <CIcon icon={cilOptions} className="text-high-emphasis-inverse" />
              </CDropdownToggle>
              <CDropdownMenu>
                <CDropdownItem>Action</CDropdownItem>
                <CDropdownItem>Another action</CDropdownItem>
                <CDropdownItem>Something else here...</CDropdownItem>
                <CDropdownItem disabled>Disabled action</CDropdownItem>
              </CDropdownMenu>
            </CDropdown>
          }
          chart={
            <CChartLine
              className="mt-3 mx-3"
              style={{ height: '70px' }}
              data={{
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [
                  {
                    label: 'My First dataset',
                    backgroundColor: 'transparent',
                    borderColor: 'rgba(255,255,255,.55)',
                    pointBackgroundColor: getStyle('--cui-info'),
                    data: [1, 18, 9, 17, 34, 22, 11],
                  },
                ],
              }}
              options={{
                plugins: {
                  legend: {
                    display: false,
                  },
                },
                maintainAspectRatio: false,
                scales: {
                  x: {
                    grid: {
                      display: false,
                      drawBorder: false,
                    },
                    ticks: {
                      display: false,
                    },
                  },
                  y: {
                    min: -9,
                    max: 39,
                    display: false,
                    grid: {
                      display: false,
                    },
                    ticks: {
                      display: false,
                    },
                  },
                },
                elements: {
                  line: {
                    borderWidth: 1,
                  },
                  point: {
                    radius: 4,
                    hitRadius: 10,
                    hoverRadius: 4,
                  },
                },
              }}
            />
          }
        />
      </CCol>
      <CCol sm={6} lg={3}>
        <CWidgetStatsA
          className="mb-4"
          color="warning"
          value={
            <>
              {'$' + currentXRPPrice.toString()}{' '}
              <span className="fs-6 fw-normal">
                (84.7% <CIcon icon={cilArrowTop} />)
              </span>
            </>
          }
          title="Ripple Coin"
          action={
            <CDropdown alignment="end">
              <CDropdownToggle color="transparent" caret={false} className="p-0">
                <CIcon icon={cilOptions} className="text-high-emphasis-inverse" />
              </CDropdownToggle>
              <CDropdownMenu>
                <CDropdownItem>Action</CDropdownItem>
                <CDropdownItem>Another action</CDropdownItem>
                <CDropdownItem>Something else here...</CDropdownItem>
                <CDropdownItem disabled>Disabled action</CDropdownItem>
              </CDropdownMenu>
            </CDropdown>
          }
          chart={
            <CChartLine
              className="mt-3"
              style={{ height: '70px' }}
              data={{
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [
                  {
                    label: 'My First dataset',
                    backgroundColor: 'rgba(255,255,255,.2)',
                    borderColor: 'rgba(255,255,255,.55)',
                    data: [78, 81, 80, 45, 34, 12, 40],
                    fill: true,
                  },
                ],
              }}
              options={{
                plugins: {
                  legend: {
                    display: false,
                  },
                },
                maintainAspectRatio: false,
                scales: {
                  x: {
                    display: false,
                  },
                  y: {
                    display: false,
                  },
                },
                elements: {
                  line: {
                    borderWidth: 2,
                    tension: 0.4,
                  },
                  point: {
                    radius: 0,
                    hitRadius: 10,
                    hoverRadius: 4,
                  },
                },
              }}
            />
          }
        />
      </CCol>
      <CCol sm={6} lg={3}>
        <CWidgetStatsA
          className="mb-4"
          color="danger"
          value={
            <>
              {'$' + currentETHPrice.toString()}{' '}
              <span className="fs-6 fw-normal">
                (-23.6% <CIcon icon={cilArrowBottom} />)
              </span>
            </>
          }
          title="Ethereum"
          action={
            <CDropdown alignment="end">
              <CDropdownToggle color="transparent" caret={false} className="p-0">
                <CIcon icon={cilOptions} className="text-high-emphasis-inverse" />
              </CDropdownToggle>
              <CDropdownMenu>
                <CDropdownItem>Action</CDropdownItem>
                <CDropdownItem>Another action</CDropdownItem>
                <CDropdownItem>Something else here...</CDropdownItem>
                <CDropdownItem disabled>Disabled action</CDropdownItem>
              </CDropdownMenu>
            </CDropdown>
          }
          chart={
            <CChartBar
              className="mt-3 mx-3"
              style={{ height: '70px' }}
              data={{
                labels: [
                  'January',
                  'February',
                  'March',
                  'April',
                  'May',
                  'June',
                  'July',
                  'August',
                  'September',
                  'October',
                  'November',
                  'December',
                  'January',
                  'February',
                  'March',
                  'April',
                ],
                datasets: [
                  {
                    label: 'My First dataset',
                    backgroundColor: 'rgba(255,255,255,.2)',
                    borderColor: 'rgba(255,255,255,.55)',
                    data: [78, 81, 80, 45, 34, 12, 40, 85, 65, 23, 12, 98, 34, 84, 67, 82],
                    barPercentage: 0.6,
                  },
                ],
              }}
              options={{
                maintainAspectRatio: false,
                plugins: {
                  legend: {
                    display: false,
                  },
                },
                scales: {
                  x: {
                    grid: {
                      display: false,
                      drawTicks: false,
                    },
                    ticks: {
                      display: false,
                    },
                  },
                  y: {
                    grid: {
                      display: false,
                      drawBorder: false,
                      drawTicks: false,
                    },
                    ticks: {
                      display: false,
                    },
                  },
                },
              }}
            />
          }
        />
      </CCol>
    </CRow>
  )
}

export default WidgetsDropdown
