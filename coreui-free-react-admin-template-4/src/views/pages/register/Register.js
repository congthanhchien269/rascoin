import React, { useEffect, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'
import { toast, ToastContainer } from 'react-toastify'
import { useHistory } from 'react-router'
import { domain } from 'src/assets/constant'

const Register = () => {
  const history = useHistory()
  const [error, setError] = React.useState({})
  const [errorUser, setErrorUser] = React.useState('')
  const [errorFullname, setErrorFullname] = React.useState('')
  const [errorEmail, setErrorEmail] = React.useState('')
  const [errorPassword, setErrorPassword] = React.useState('')
  const [errorRePassword, setErrorRePassword] = React.useState('')
  const [click, setClick] = React.useState(false)
  const [check, setCheck] = React.useState(false)
  const [datau, setDatau] = React.useState({
    name: '',
    fullname: '',
    email: '',
    password: '',
    re_password: '',
  })

  const notify = () => {
    const errors = {}
    const pattern = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/
    if (datau.name.includes(' ')) {
      setErrorPassword('Password is bad format')
      errors.error4 = '4'
    }
    if (datau.fullname === '') {
      setErrorFullname('Full Name cannot be empty')
      errors.error9 = '9'
    }
    if (datau.email.match(pattern)) {
    } else {
      setErrorEmail('Email is bad format')
      errors.error5 = '5'
    }

    if (datau.password === '') {
      setErrorPassword('Password cannot be empty')
      errors.error10 = '10'
    }
    if (datau.password.length > 11) {
      setErrorPassword('Password must not exceed 11 characters ')
      errors.error11 = '11'
    }
    if (datau.password.length < 6) {
      setErrorPassword('Password must be at least 6 characters ')
      errors.error12 = '12'
    }

    if (datau.re_password === '') {
      setErrorRePassword('Password cannot be empty')
      errors.error13 = '13'
    } else if (datau.re_password !== datau.password) {
      setErrorRePassword('Password does not match')
      errors.error13 = '14'
    }
    if (datau.fullname.length > 30) {
      setErrorFullname('Username must not exceed 30 characters ')
      errors.error9 = '9'
    }
    if (datau.fullname.length < 8) {
      setErrorFullname('Username must be at least 8 characters ')
      errors.error8 = '8'
    }
    if (datau.name.length > 16) {
      setErrorUser('Username must not exceed 16 characters ')
      errors.error11 = '11'
    }
    if (datau.name.length < 8) {
      setErrorUser('Username must be at least 8 characters ')
      errors.error12 = '12'
    }

    return errors
  }

  useEffect(() => {
    if (Object.keys(error).length === 0 && click) {
      toast.success('Your account has been created ')
      const data = {
        user_name: datau.name,
        full_name: datau.fullname,
        email: datau.email,
        password: datau.password,
      }
      fetch(`${domain}/user/register`, {
        method: 'POST',
        body: JSON.stringify(data),
      })
        .then((response) => response.json())
        .then((data) => {
          console.log('Success:', data)
        })
        .catch((error) => {
          console.error('Error:', error)
        })
      history.push('/')
    }
    console.log(error)
  }, [error])

  useEffect(() => {
    if (check === true) {
      setError(notify)
      setCheck(false)
    }
  }, [check])

  const errorCheck = () => {
    setErrorPassword(null)
    setErrorRePassword(null)
    setErrorUser(null)
    setErrorEmail(null)
    setErrorFullname(null)
    setClick(true)
    setCheck(true)
  }
  function handle(e) {
    const newData = { ...datau }
    newData[e.target.id] = e.target.value
    setDatau(newData)
    console.log(newData)
  }
  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <div>
        <ToastContainer />
      </div>
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={9} lg={7} xl={6}>
            <CCard className="mx-4">
              <CCardBody className="p-4">
                <CForm>
                  <h1>Register</h1>
                  <p className="text-medium-emphasis">Create your account</p>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilUser} />
                    </CInputGroupText>
                    <CFormInput
                      placeholder="Username"
                      autoComplete="username"
                      id="name"
                      onChange={(e) => handle(e)}
                    />
                  </CInputGroup>
                  {errorUser && (
                    <CInputGroup style={{ height: '15px' }}>
                      <p style={{ marginTop: '-13px', marginLeft: '2px', color: 'red' }}>
                        {errorUser}
                      </p>
                    </CInputGroup>
                  )}
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilUser} />
                    </CInputGroupText>
                    <CFormInput
                      placeholder="Full name"
                      autoComplete="fullname"
                      id="fullname"
                      onChange={(e) => handle(e)}
                    />
                  </CInputGroup>
                  {errorFullname && (
                    <CInputGroup style={{ height: '15px' }}>
                      <p style={{ marginTop: '-13px', marginLeft: '2px', color: 'red' }}>
                        {errorFullname}
                      </p>
                    </CInputGroup>
                  )}
                  <CInputGroup className="mb-3">
                    <CInputGroupText>@</CInputGroupText>
                    <CFormInput
                      placeholder="Email"
                      autoComplete="email"
                      id="email"
                      onChange={(e) => handle(e)}
                    />
                  </CInputGroup>
                  {errorEmail && (
                    <CInputGroup style={{ height: '15px' }}>
                      <p style={{ marginTop: '-13px', marginLeft: '2px', color: 'red' }}>
                        {errorEmail}
                      </p>
                    </CInputGroup>
                  )}

                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilLockLocked} />
                    </CInputGroupText>
                    <CFormInput
                      type="password"
                      placeholder="Password"
                      autoComplete="new-password"
                      id="password"
                      onChange={(e) => handle(e)}
                    />
                  </CInputGroup>
                  {errorPassword && (
                    <CInputGroup style={{ height: '15px' }}>
                      <p style={{ marginTop: '-13px', marginLeft: '2px', color: 'red' }}>
                        {errorPassword}
                      </p>
                    </CInputGroup>
                  )}
                  <CInputGroup className="mb-4">
                    <CInputGroupText>
                      <CIcon icon={cilLockLocked} />
                    </CInputGroupText>
                    <CFormInput
                      type="password"
                      placeholder="Confirm password"
                      autoComplete="new-password"
                      id="re_password"
                      onChange={(e) => handle(e)}
                    />
                  </CInputGroup>
                  {errorRePassword && (
                    <CInputGroup style={{ height: '15px' }}>
                      <p style={{ marginTop: '-13px', marginLeft: '2px', color: 'red' }}>
                        {errorRePassword}
                      </p>
                    </CInputGroup>
                  )}
                  <div className="d-grid">
                    <CButton color="success" onClick={() => errorCheck()}>
                      Create Account
                    </CButton>
                  </div>
                </CForm>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Register
