import React, { lazy, useEffect } from 'react'
import axios from 'axios'
import ReactApexChart from 'react-apexcharts'
import { CButton, CButtonGroup, CCard, CCardBody, CCol, CRow, CFormInput } from '@coreui/react'
import 'bootstrap-daterangepicker/daterangepicker.css'
import CIcon from '@coreui/icons-react'
// import Button from '@mui/material/Button'
import { cilCloudDownload } from '@coreui/icons'
import { cilBell } from '@coreui/icons'
import DateRangePicker from 'react-bootstrap-daterangepicker'
import moment from 'moment'
import { Calendar } from 'react-feather'
import { domain } from '../../assets/constant.js'
import { CFormSelect } from '@coreui/react'

const WidgetsDropdown = lazy(() => import('../components/widgets/WidgetsDropdown.js'))

const Dashboard = () => {
  const [currUserId, setCurrUserId] = React.useState(1)
  const [coin, setCoin] = React.useState('bitcoin')
  const [dateStart, setdateStart] = React.useState(null)
  const [dateEnd, setdateEnd] = React.useState(null)
  const [content, setContent] = React.useState(null)
  const [notifyPrice, setnotifyPrice] = React.useState(null)
  const [notifyPriceMax, setnotifyPriceMax] = React.useState(null)
  const [dataDate, setDataDate] = React.useState([
    '1/22/20',
    '2/1/20',
    '2/15/20',
    '3/1/20',
    '3/15/20',
    '4/1/20',
    '4/15/20',
    '5/1/20',
    '5/7/20',
  ])
  const [dataPrice, setDataPrice] = React.useState([
    555, 12038, 69030, 88369, 167466, 932638, 2055423, 3343777, 3845718,
  ])
  const [tracking, setTracking] = React.useState(false)
  const [iTrack, setITrack] = React.useState(false)
  const [content2, setContent2] = React.useState(null)
  const [accessToken, setAccessToken] = React.useState(null)
  const [coinId, setCoinId] = React.useState(null)
  const random = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }
  const onDateSelectChange = (e, picker) => {
    setdateStart(moment(picker.startDate._d).unix())
    setdateEnd(moment(picker.endDate._d).unix())
    setTracking(false)
  }
  // const header = {
  //   key: 'access_token',
  //   value: accessToken,
  //   type: 'text',
  // }
  const header = { access_token: accessToken, 'Content-Type': 'application/json' }
  const fetchAdvertiser = async (header) => {
    let query = {
      filters: {
        $and: [
          { timestamp: { $gte: dateStart } },
          { timestamp: { $lte: dateEnd } },
          { coin_id: { $eq: coinId } },
        ],
      },
    }
    let query_string = JSON.stringify(query)
    const { data } = await axios.get(`${domain}/api/v1/crypto_price?q=${query_string}`, header)
    setContent(data)
  }
  const fetchCurrentUserID = async () => {
    const { data } = await axios.get(`${domain}/user/current_user`)
    setCurrUserId(data.id)
  }
  const fetchCurrentCoinID = async (headers, coin) => {
    let query = { filters: { gecko_coin_id: { $eq: coin } } }
    let query_string = JSON.stringify(query)
    const { data } = await axios.get(`${domain}/api/v1/crypto_currency?q=${query_string}`)
    setCoinId(data.objects[0].id)
  }
  const postNotify = async () => {
    let query = {
      notify_price_at: parseFloat(notifyPrice),
      notify_type: 0,
      price_status: 0,
      coin_id: coinId,
      user_id: 1,
      currency_id: 1,
    }
    fetchCurrentUserID()
    console.log('coinID', coinId)
    const { data } = await axios.post(`${domain}/api/v1/notification`, query, {
      headers: header,
    })
  }
  const postNotify2 = async () => {
    let query = {
      notify_price_at: parseFloat(notifyPriceMax),
      notify_type: 1,
      price_status: 1,
      coin_id: coinId,
      user_id: 1,
      currency_id: 1,
    }
    console.log('coinID', coinId)
    fetchCurrentUserID()
    const { data } = await axios.post(`${domain}/api/v1/notification`, query, {
      headers: header,
    })
  }
  useEffect(() => {
    if (dateStart !== null && dateEnd !== null) {
      fetchAdvertiser(header)
    }
  }, [dateStart, dateEnd])

  useEffect(() => {
    if (content !== null && tracking === false) {
      const optionDataPrice = content?.objects.reduce((acc, crr) => {
        return [...acc, crr.price]
      }, [])
      const optionDataDate = content?.objects.reduce((acc, crr) => {
        const ahihi = moment(crr.timestamp * 1000).format('MM.DD.yyyy')
        return [...acc, ahihi]
      }, [])
      setDataDate(optionDataDate)
      setDataPrice(optionDataPrice)
      setdateStart(null)
      setdateEnd(null)
    }
  }, [content])

  useEffect(() => {
    const x = localStorage.getItem('access_token')
    setAccessToken(x)
  }, [])

  useEffect(() => {
    if (content2 !== null && tracking === true) {
      console.log('content2', content2)
      console.log('coin', coin)
      console.log('coinId', coinId)
      const optionDataPrice = content2[coin].reduce((acc, crr) => {
        return [...acc, crr.price]
      }, [])
      // console.log('content2', content2)
      const optionDataDate = content2[coin].reduce((acc, crr) => {
        // const ahihi = moment(crr.time_stamp * 1000).format('MMMM Do YYYY, h:mm:ss a')
        // const ahihi = moment(crr.time_stamp * 1000).format('MM.DD.yyyy')
        return [...acc, crr.datetime]
      }, [])
      // console.log('optionDataPrice', optionDataPrice)
      // console.log('optionDataDate', optionDataDate)
      setDataDate(optionDataDate)
      setDataPrice(optionDataPrice)
    }
  }, [content2])

  const fetchAdvertiser2 = async () => {
    const { data } = await axios.get(
      `${domain}/recent_prices?coin_ids=bitcoin,ripple,binancecoin,cardano,ethereum`,
      header,
    )
    setContent2(data)
  }
  const handleSetTracking = () => {
    setTracking(true)
    // setDataPrice([])
    // setDataDate([])
  }
  const handleNotify = () => {
    if (notifyPrice === null || notifyPrice === '') {
      alert('Please enter the price you want to be notified')
    } else {
      postNotify()
      alert(`We will send an email when the cryptocurrency price is under ${notifyPrice}`)
    }
  }
  const handleNotify2 = () => {
    if (notifyPriceMax === null || notifyPriceMax === '') {
      alert('Please enter the price you want to be notified')
    } else {
      postNotify2()
      alert(`We will send an email when the cryptocurrency price is above ${notifyPriceMax}`)
    }
  }

  useEffect(() => {
    if (tracking === true) {
      fetchAdvertiser2()
      setITrack(
        setInterval(() => {
          fetchAdvertiser2()
        }, 5000),
      )
    }
    if (tracking === false) {
      clearInterval(iTrack)
    }

    return clearInterval(iTrack)
  }, [tracking])

  const series = [
    {
      name: 'Price',
      data: dataPrice,
    },
  ]
  const options = {
    dataLabels: {
      enabled: false,
    },
    stroke: {
      curve: 'smooth',
    },
    xaxis: {
      type: 'datetime',
      categories: dataDate,
    },
    tooltip: {
      x: {
        format: 'dd/MM/yy',
      },
    },
  }
  return (
    <>
      <WidgetsDropdown />
      <div style={{ paddingBottom: '10px' }}>
        <CFormSelect
          aria-label="Select coin to track"
          onChange={(e) => {
            setCoin(e.target.value)
            fetchCurrentCoinID(header, coin)
          }}
        >
          <option>Select Coin</option>
          <option value="bitcoin">BTC</option>
          <option value="binancecoin">BNB</option>
          <option value="ripple">XRP</option>
          <option value="cardano">ADA</option>
          <option value="ethereum">ETH</option>
        </CFormSelect>
      </div>
      <div style={{ display: 'flex', width: '100%' }}>
        <CFormInput
          style={{ width: '200px' }}
          placeholder="Max Price"
          id="name"
          type="number"
          onChange={(e) => setnotifyPriceMax(e.target.value)}
        />
        <CButton
          style={{ backgroundColor: 'yellow', color: 'black' }}
          onClick={() => handleNotify2()}
        >
          <CIcon icon={cilBell} />
        </CButton>
      </div>
      <br />
      <div style={{ display: 'flex', width: '100%' }}>
        <CFormInput
          style={{ width: '200px' }}
          placeholder="Min Price"
          id="name"
          type="number"
          onChange={(e) => setnotifyPrice(e.target.value)}
        />
        <CButton
          style={{ backgroundColor: 'yellow', color: 'black' }}
          onClick={() => handleNotify()}
        >
          <CIcon icon={cilBell} />
        </CButton>
        <div style={{ display: 'inline-block', marginLeft: 'auto' }}>
          Current Price : {content2?.bitcoin[content2?.bitcoin.length - 1].price}{' '}
        </div>
      </div>
      <CCard className="mb-4">
        <CCardBody>
          <CRow>
            <CCol sm={5}>
              <div>
                {tracking ? (
                  <div>
                    <CButton onClick={() => setTracking(false)}>Tracking mode : on</CButton>
                    <span style={{ marginLeft: '10px' }}>Tracking cryptocurrency price</span>
                  </div>
                ) : (
                  <CButton className="btn btn-secondary" onClick={() => handleSetTracking()}>
                    Tracking mode : off
                  </CButton>
                )}
              </div>
            </CCol>
            <CCol sm={7} className="d-none d-md-block">
              <CButton color="primary" className="float-end">
                <CIcon icon={cilCloudDownload} />
              </CButton>
              <CButtonGroup className="float-end me-3">
                <DateRangePicker
                  onApply={(e, picker) => onDateSelectChange(e, picker)}
                  initialSettings={{
                    ranges: {
                      Today: [moment().toDate(), moment().toDate()],
                      Yesterday: [
                        moment().subtract(1, 'days').toDate(),
                        moment().subtract(1, 'days').toDate(),
                      ],
                      'Last 7 Days': [moment().subtract(6, 'days').toDate(), moment().toDate()],
                      'Last 14 Days': [moment().subtract(13, 'days').toDate(), moment().toDate()],
                      'Last 30 Days': [moment().subtract(29, 'days').toDate(), moment().toDate()],
                      'Last 90 Days': [moment().subtract(89, 'days').toDate(), moment().toDate()],
                      'This Month': [
                        moment().startOf('month').toDate(),
                        moment().endOf('month').toDate(),
                      ],
                      'Last Month': [
                        moment().subtract(1, 'month').startOf('month').toDate(),
                        moment().subtract(1, 'month').endOf('month').toDate(),
                      ],
                      'This Year': [
                        moment().subtract(1, 'month').startOf('year').toDate(),
                        moment().toDate(),
                      ],
                    },
                    alwaysShowCalendars: true,
                    showCustomRangeLabel: false,
                  }}
                >
                  <button
                    className="btn-sm bg-transparent text-dark"
                    style={{
                      border: '1px solid #e2e7f1',
                      float: 'right',
                      marginTop: '10px',
                      marginRight: '10px',
                    }}
                  >
                    <Calendar size="14" /> Choose date
                  </button>
                </DateRangePicker>
              </CButtonGroup>
            </CCol>
          </CRow>
          <div
            style={{
              backgroundColor: 'white',
              textAlign: 'center',
            }}
          >
            <ReactApexChart options={options} series={series} type="line" height={400} />
          </div>
        </CCardBody>
      </CCard>
    </>
  )
}

export default Dashboard
